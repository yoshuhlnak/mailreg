﻿using System.ComponentModel.DataAnnotations.Schema;
using MailRegistration.Model;

namespace DAL
{
    public class Letter : LetterBase
    {
        [DatabaseGenerated( DatabaseGeneratedOption . Identity )]
        public override int Id { get; set; }
        public int ReceiveEmployeeId { get; set; }
        public int SendEmployeeId { get; set; }
        public virtual Employee ReceiveEmployee { get; set; }
        public virtual Employee SendEmployee { get; set; }

        [NotMapped]
        public override EmployeeBase Receiver 
        {
            get { return ReceiveEmployee;} 
            set{ ReceiveEmployee = value as Employee;} 
        }
        [NotMapped]
        public override EmployeeBase Sender 
        {
            get { return SendEmployee; }
            set{ SendEmployee = value as Employee; } 
        }
    }
}