﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using MailRegistration.Model;

namespace DAL
{
    public class Employee : EmployeeBase
    {
        public Employee()
        {
            ReceiverLetter = new List<Letter>();
            SenderLetter = new List<Letter>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override int Id { get; set; }

        public virtual ICollection<Letter> ReceiverLetter { get; set; }

        public virtual ICollection<Letter> SenderLetter { get; set; }
    }
}