﻿using System;
using System.Data.Entity;
using DAL.ContextRelated;

namespace DAL
{
    public class DataContext : DbContext
    {
        public DataContext() : base("MailRegistration")
        {
            // DO NOT REMOVE
            // next lines is workaround for InvalidOperationException: No Entity Framework provider found for the ADO.NET provider with invariant name 'System.Data.SqlClient'
            var type = typeof(System.Data.Entity.SqlServer.SqlProviderServices);
            if (type == null)
                throw new Exception("Do not remove, ensures static reference to System.Data.Entity.SqlServer");

            Configuration . LazyLoadingEnabled = false;
            Configuration . ProxyCreationEnabled = true;

            Database . SetInitializer( new DataContextInitializer() );
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // create foreign key Letter.Receiver - Employee
            modelBuilder.Entity<Letter>()
                .HasRequired<Employee>(l=>l.ReceiveEmployee)
                .WithMany(e=>e.ReceiverLetter)
                .HasForeignKey(k => k.ReceiveEmployeeId)
                .WillCascadeOnDelete(false);

            // create foreign key Letter.Sender - Employee
            modelBuilder.Entity<Letter>()
                .HasRequired<Employee>(letter => letter.SendEmployee)
                .WithMany(e => e.SenderLetter)
                .HasForeignKey(k => k.SendEmployeeId)
                .WillCascadeOnDelete(false);
            // exclude properties from mapping 
            modelBuilder.Entity<Letter>().Ignore(e => e.Sender);
            modelBuilder.Entity<Letter>().Ignore(e => e.Receiver);

            // set varchar lenghts
            modelBuilder . Entity<Letter>()
                . Property( letter => letter . Contents ) . HasColumnType( "VARCHAR" ) . IsMaxLength();
            modelBuilder . Entity<Letter>()
                . Property( letter => letter . Name ) . HasColumnType( "VARCHAR" ) . HasMaxLength( 1000 );
            modelBuilder . Entity<Employee>()
                . Property( e => e . Name ) . HasColumnType( "VARCHAR" ) . HasMaxLength( 100 );
        }

        public DbSet<Letter> Letters { get; set; }
        public DbSet<Employee> Employees { get; set; }
    }
}