﻿using System.Collections.Generic;
using System.Data.Entity;

namespace DAL.ContextRelated
{
    public class DataContextInitializer : CreateDatabaseIfNotExists<DataContext> 
    {
        protected override void Seed(DataContext context)
        {

            IList<Employee> defaultEmployees = new List<Employee>();
            defaultEmployees . Add( new Employee() { Name = "Аминев Булат Даянович" } );
            defaultEmployees . Add( new Employee() { Name = "Алексейчук Андрей Емельянович" } );
            defaultEmployees . Add( new Employee() { Name = "Буркова Ирина Владимировна" } );
            defaultEmployees . Add( new Employee() { Name = "Вотрина Марина Игоревна" } );
            defaultEmployees . Add( new Employee() { Name = "Гончаренко Станислав Степанович" } );
            defaultEmployees . Add( new Employee() { Name = "Губанов Дмитрий Алексеевич" } );
            defaultEmployees . Add( new Employee() { Name = "Ермилова Татьяна Вячеславовна" } );
            defaultEmployees . Add( new Employee() { Name = "Иванов Николай Николаевич" } );
            defaultEmployees . Add( new Employee() { Name = "Ицкович Эммануил Львович" } );
            defaultEmployees . Add( new Employee() { Name = "Карнышева Элина Андреевна" } );
            defaultEmployees . Add( new Employee() { Name = "Козырев Дмитрий Владимирович" } );
            defaultEmployees . Add( new Employee() { Name = "Кузнецов Сергей Константинович" } );
            defaultEmployees . Add( new Employee() { Name = "Лапицкий Антон Дмитриевич" } );
            defaultEmployees . Add( new Employee() { Name = "Мараканов Игорь Николаевич" } );
            defaultEmployees . Add( new Employee() { Name = "Никулина Ирина Владимировна" } );

            foreach (var employee in defaultEmployees)
            {
                context.Employees.Add(employee);
            }

            base.Seed(context);
        }
    }
}