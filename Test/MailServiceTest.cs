﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using MailRegistration.Exception;
using MailRegistration.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Test.MailServiceReference;

namespace Test
{
    [TestClass]
    public class MailServiceTest
    {
        private static IList<EmployeeBase> _employeeList = null;
        private static IList<string> _nameList = new List<string>();
        private static IList<string> _textList = new List<string>();

        [ClassInitialize]
        public static void Initilization(TestContext context)
        {
            // инициализация наименований
            _nameList.Add("Типичные ошибки начинающего технического директора в ИТ — мнения экспертов");
            _nameList.Add("Как развивается услуга сохранения номера абонента при смене оператора");
            _nameList.Add("Типографируем названия организаций");
            _nameList.Add("Пастильда — открытый аппаратный менеджер паролей");
            _nameList.Add("Повышение энергоэффективности дата-центров: опыт Apple, Google, Microsoft, Active Power и Burland Energy");
            _nameList.Add("Типичные ошибки начинающего технического директора в ИТ — мнения экспертов");
            _nameList.Add("Типичные ошибки начинающего технического директора в ИТ — мнения экспертов");
            _nameList.Add("Типичные ошибки начинающего технического директора в ИТ — мнения экспертов");
            _nameList.Add("Типичные ошибки начинающего технического директора в ИТ — мнения экспертов");
            _nameList.Add("Типичные ошибки начинающего технического директора в ИТ — мнения экспертов");


            // инициализация содержаний
#region Text initialization
            _textList.Add(@"От некоторых сотрудников ИТ-компаний до сих пор можно услышать такую реплику: «Я не совсем понимаю точное значение должности Технический директор». Как отметил в предельно простой форме один из пользователей «Тостера», «CTO — технический человек, который что-то понимает в бизнесе». Если рассматривать это понятие чуть шире, то можно сказать, что он балансирует на стыке между разработкой ИТ-продуктов с командой технических специалистов и принятием бизнес-решений совместно с менеджерами.

Соответственно, для специалистов, желающих занять позицию технического директора в ИТ, существует, как минимум два пути:

    стандартный — «Developer -> Senior -> Team lead -> CTO»;
    гуманитарный – «PM -> Senior PM -> CTO».


Безусловно, в случае второго варианта понимать технические нюансы техническому директору может быть сложнее.

Но достигнув желаемого, в целом, опытный специалист переходит как бы в разряд начинающих. Он становится этаким Junior-CTO и сталкивается с новыми вызовами.

О том, какие ошибки и подводные камни ожидают новоиспеченных технических директоров в ИТ-сфере, мы попросили рассказать экспертов отрасли.

Виктор Шабуров, предприниматель, инвестор и один из основателей компаний Looksery Inc., Handster Inc. и SPB Software

Насколько часто начинающий технический директор ошибается с дедлайнами?");

            _textList.Add(@"Почему пользователи меняют операторов?

Чаще всего перейти к другому оператору пользователей заставляет так называемая «усталость от бренда». Накапливаются негативные впечатления, связанные с платными подписками на контент, качеством связи и нюансами оплаты.

На удовлетворенности клиентов также сказывается снижение качества связи с ростом абонентской базы. Возможность оставить прежний номер позволяет легко решиться на переход к другому оператору.

Картина развития MNP в мире

Впервые возможность сохранить номер реализовали в Сингапуре в 1997 году, но попытка оказалась неудачной. Перенести удавалось только звонки и SMS, а любая другая услуга проходила через сеть предыдущего оператора. Здесь услуга была реализована полноценно только в 2008 году.

Первую удачную попытку предприняли тоже в 1997 году, в Нидерландах. В 1999 к ним присоединились Великобритания и Гонконг. В настоящее время услуга MNP доступна более чем в 70 странах мира.

Популярность услуги в разных странах сильно отличается. Например, в Финляндии, где сохранить номер можно бесплатно, доля перенесённых номеров за один год превысила 20%. В Нидерландах, где MNPэта услуга обойдется в 9 Евро — 12% от среднего ARPU (Average revenue per user, средняя выручка на одного пользователя за месяц), доля перенесённых номеров — 7%. А в Германии «взять» с собой номер стоит 29 Евро, поэтому там услуга MNP совсем непопулярна — меньше 1%. Опережает всех по числу перенесенных номеров Сингапур, где 95% абонентов поменяли оператора уже в первый год.

На популярность MNP оказывают влияние несколько факторов:

Развитие рынка мобильной связи в стране

Услуга MNP оказывается наиболее популярна в странах, мобильный рынок которых находится в ранней стадии развития. Самые благоприятные условия для ее развития — низкое проникновение мобильной связи, невысокое качество услуг и неразвитая конкуренция. Один из лидеров по популярности MNP – Финляндия — на момент внедрения услуги в 2003 году показывала уровень проникновения мобильной связи около 90% (для сравнения: в России этот показатель сейчас составляет около 180%), а оператор Sonera доминировал с долей рынка 56%.

Напротив, Япония на момент принятия MNP в 2006 году отличалась очень высокой конкуренцией в секторе мобильной связи, и услуга оказалась непопулярной, а доля перенесенных номеров составила всего 0,2% (против ожидаемых 30%).

Условия перехода и количество бюрократических барьеров

Популярность MNP, конечно, в значительной степени зависит от стоимости переноса, необходимости личного присутствия и срока, за который можно осуществить переход. Хотя востребованность услуги не всегда обратно пропорциональная стоимости. Например, в Великобритании, где она предоставляется бесплатно, доля перенесенных номеров значительно ниже, чем в Дании, где за MNP нужно заплатить около 10 евро.

Степень лояльности абонентов

Традиционно доля перенесенных номеров является невысокой в странах, в которых принято заключать долгосрочные (на год и больше) контракты с мобильным оператором. Такая практика сложилась, например, в США и Японии. В этих странах операторы предлагают большие скидки абонентам при заключении годового контракта, а расторжение такого контракта подразумевает высокие штрафы.");

            _textList.Add(@"Единственное о чем стоит помнить при разборе — вложенные кавычки. С ними нужно обходиться бережно и при вёрстке обязательно их свешивать.

Лирическое отступление

Иностранные компании верстаются в обратном порядке: Niantic, Inc.; Public Image, Ltd и никто не помешает нам регистрировать свои компании с расположением частей в таком порядке: Моя фирма, ООО.

Возможно это поломает бухгалтерский и налоговый софт, но, мне кажется, уже давно наступило время что-то поменять.

Как разобрать название?

    Вспоминая о том, что обычно название не вводят, а копируют и вставляют из полученной карточки предприятия, нужно убрать из него все лишние пробельные символы.
    Заменить все расшифровки организационно-правовых форм и видов ответственности на аббревиатуры и вынести их в конец названия.
    Привести кавычки к единому виду, убрать те, что обрамляют название целиком и оставить только вложенные.



Я написал небольшой скрипт (beautyCo.js), который выполняет эти действия. Он не претендует на гениальность, но сможет быть полезен в простых случаях или стать отправной точкой для чего-то большего.

Грабли

В процессе работы над скриптом столкнулся ещё с одной проблемой — латинские символы «дублёры».

В целях экономии времени, пользователи не утруждают себя перенабором уже начатых в латинской раскладке фраз, они переключаются в кириллицу и продолжают писать дальше:

    OOO «Paдyгa»


В этом названии всего два кириллических знака, а следовательно и поиск, и разбор названия будет затруднителен, хотя в печати и на экране всё как-будто в порядке.

Пришлось написать небольшой скрипт (antiPE.js), который меняет символы «дублёры» на обычные. Он не умеет распознавать язык текста, да ему это и не нужно, т.к. в Российской федерации все названия организаций должны быть на русском языке (согласно пункту 3 статьи 1473 ГК РФ).");

            _textList.Add(@"Немало заметок и обсуждений посвящены непростому вопросу безопасного хранения паролей, тема интересная и, похоже, актуальной будет ещё долго. Существуют различные программные решения для хранения паролей, о них довольно часто пишут на Хабре (например тут и вот тут), однако многим из них, как нам кажется, в той или иной степени свойственны следующие недостатки:

    закрытый код снижает доверие и вероятность оперативного устранения уязвимостей
    для автозаполнения нужно ставить дополнительный софт
    после ввода мастер-пароля вся база открыта и доступна, в том числе для вредоносного ПО, что особенно актуально на недоверенных устройствах
    использование мобильных приложений для хранения паролей все равно подразумевает ручной ввод с клавиатуры, например когда требуется залогиниться на стационарном ПК
    автозаполнение невозможно в некоторых случаях (в bios, консоли)


Мы пришли к выводу, что наиболее удобным решением будет простой и недорогой девайс, позволяющий аппаратно хранить и вводить логины/пароли на любые устройства, без установки какого-либо ПО.");
            _textList.Add(@"Компания Bloom Energy выступила в качестве поставщика топливных элементов. Данная компания уже сотрудничала с Apple в Северной Каролине, где развернула генерирующие блоки неподалеку от кампуса дата-центра.

Возле здания Campus 2 инженеры Bloom Energy планируют развернуть 4-мегаваттные блоки, дополняющиеся солнечными батареями и массивом аккумуляторов. Скорее всего это будет супер современное решение Bloom Energy Server 5 с эффективностью, доходящей до 65%. И с точки зрения плотности мощности новое решение обещает почти в два раза превзойти своих предшественников.

Практика повышения энергоэффективности и снижения углеродного следа ЦОД от Google

В 2015 году компания Google сделала заявление о намерении расширить кампус ЦОД в центральной части острова Тайвань. Это понадобилось для ускорения экспансии на быстрорастущем рынке интернет-услуг Азии. И поисковый гигант также выбрал локальные генерирующие мощности на базе возобновляемых источников энергии. Подобный подход позволяет минимизировать углеродный след дата-центров, одновременно повышая энергоэффективность их инфраструктуры путем снижения потерь при передаче электроэнергии. ");
#endregion
        }
        /// <summary>
        /// возвращает случайный номер
        /// </summary>
        /// <returns></returns>
        private int GetRandomNum(int min= 1, int max = 10 )
        {
            var rand = new Random();
            return rand.Next(min, max);
        }
        /// <summary>
        /// получение случайного сотрудника
        /// </summary>
        /// <returns></returns>
        private EmployeeBase GetRandomEmployee()
        {
            if( _employeeList == null )
                SetEmploeeList();
            return _employeeList.ElementAt(GetRandomNum());
        }
        /// <summary>
        /// получение случайного наименования
        /// </summary>
        /// <returns></returns>
        private string GetRandomName()
        {
            return _nameList.ElementAt(GetRandomNum(0, 4));
        }
        /// <summary>
        /// получение случайного текста 
        /// </summary>
        /// <returns></returns>
        private string GetRandomText()
        {
            return _textList.ElementAt(GetRandomNum(0, 4));
        }
        /// <summary>
        /// Получение случайного письма из сервиса
        /// </summary>
        /// <param name="service"></param>
        /// <returns></returns>
        LetterBase GetRandomLetter(DataServiceClient service)
        {
            var letters = GetAllLetters();
            Assert.IsNotNull(letters, "There is no collection to delete from");
            Assert.AreNotEqual(letters.Count, 0, "There are no letters to delete - collection empty");
            return letters.ElementAt(GetRandomNum(0, letters.Count));
        }
        /// <summary>
        /// получение всех писем
        /// </summary>
        /// <returns></returns>
        private List<LetterBase> GetAllLetters()
        {
            var service = new DataServiceClient();
            var letters = service.GetAllLetters();
            return letters;
        }
        /// <summary>
        /// Заполняет список сотрудников
        /// </summary>
        [TestMethod()]
        public void SetEmploeeList()
        {
            var service = new MailServiceReference.DataServiceClient();
            _employeeList = service.GetAllEmployees();
            Assert.IsNotNull( _employeeList, "Failed to receive employee list" );
        }
        /// <summary>
        /// Добавление письма
        /// </summary>
        [TestMethod()]
        [Ignore] 
        public void AddLetter()
        {
            var sender = GetRandomEmployee();
            var receiver = GetRandomEmployee();
            string name = GetRandomName();
            string text = GetRandomText();
            DateTime date = DateTime.Now;
            var service = new DataServiceClient();
            LetterBase letter = new LetterBase()
            {
                Name = name,
                Contents = text,
                Date = date,
                Receiver = receiver,
                Sender = sender
            };
            LetterBase newLetter = null; 
            try
            {
                newLetter = service.SaveLetter(letter);
            }
            catch (FaultException<DataBaseFault> exception)
            {
                Assert.Fail(
                    string.Format("Adding new letter resulted in exception of type {0} with reason: {1}\nDetail: {2}",
                        exception.GetType().Name, exception.Reason, exception.Detail));
            }
            Assert.IsNotNull(newLetter, "While adding new letter SaveLetter method returned null");
            Assert.AreEqual(letter.Contents, newLetter.Contents, "Saved letter contents does not coincide with initial");
            Assert.AreEqual(letter.Name, newLetter.Name, "Saved letter name does not coincide with initial");
            Assert.AreEqual(letter.Receiver.Id, newLetter.Receiver.Id, "Saved letter receiver does not coincide with initial");
            Assert.AreEqual(letter.Sender.Id, newLetter.Sender.Id, "Saved letter sender does not coincide with initial");
            Assert.AreEqual(letter.Date, newLetter.Date, "Saved letter date does not coincide with initial");
        }
        /// <summary>
        /// проверка получения всех писем
        /// </summary>
        [TestMethod]
        public void LetterRetrieval()
        {
            var letters = GetAllLetters();
            Assert.IsNotNull(letters, "Letters retrieval unsuccessful");
        }

        /// <summary>
        /// Удаление письма
        /// </summary>
        [TestMethod]
        public void DeleteLetter()
        {
            var service = new DataServiceClient();
            var letters = GetAllLetters();
            Assert.IsNotNull( letters, "There is no collection to delete from" );
            Assert.AreNotEqual(letters.Count, 0, "There are no letters to delete - collection empty");
            var letter = GetRandomLetter(service);
            try
            {
                service.DeleteLetter(letters.ElementAt(GetRandomNum(0, letters.Count)));
            }
            catch (Exception exception)
            {
                Assert.Fail(string.Format("Deleting letter caused and axception {0} with message {1}",
                    exception.GetType().Name, exception.Message));
            }
            letters = GetAllLetters();
            CollectionAssert.DoesNotContain( letters, letter, "Letter has not been deleted from source" );
        }

        [TestMethod()]
        public void EditRandomLetter()
        {
            var service = new DataServiceClient();
            var letter = GetRandomLetter(service);
            // save copy of letter to refer to
            var initialLetter = new LetterBase()
            {
                Id = letter.Id,
                Name = letter.Name,
                Contents = letter.Contents,
                Date = letter.Date,
                Receiver = letter.Receiver,
                Sender = letter.Sender
            };

            bool employee_check, name_check, text_check;
            employee_check = text_check = name_check = true;
            // получаем новые значения полей
            EmployeeBase receiver = null ;
            EmployeeBase sender = null;

            // проверка не имеет смысла если не откуда брать сотрудников, или если контейнер содержит менее 2х объектов
            if (_employeeList != null && _employeeList.Count > 1)
            {
                // в цикле пытаемся найти отличающегося сотрудника 
                while (receiver == null || receiver == letter.Receiver)
                    receiver = GetRandomEmployee();
                // в цикле пытаемся найти отличающегося сотрудника 
                while( sender == null || sender == letter.Sender ) 
                    sender = GetRandomEmployee();

                // устанавливаем новые значения
                letter.Receiver = receiver;
                letter.Sender = sender;
            }
            else
                employee_check = false;
            //изменяем дату
            letter.Date=letter.Date.AddYears(-5);
            string name = null;
            // проверка не имеет смысла если не откуда брать сотрудников, или если контейнер содержит менее 2х объектов
            if (_nameList != null && _nameList.Count > 1)
            {
                // в цикле пытаемся найти отличающееся наименование
                while (string.IsNullOrEmpty(name) || name == letter.Name)
                    name = GetRandomName();

                // устанавливаем новые значения
                letter.Name = name;
            }
            else
                name_check = false;
            string text = null;
            // проверка не имеет смысла если не откуда брать сотрудников, или если контейнер содержит менее 2х объектов
            if (_textList != null && _textList.Count > 1)
            {
                // в цикле пытаемся найти отличающееся наименование
                while (string.IsNullOrEmpty(text) || text == letter.Contents)
                    text = GetRandomText();

                // устанавливаем новые значения
                letter.Contents = text;
            }
            else
                text_check = false;
            LetterBase savedLetter = null;
            try
            {
                savedLetter = service.SaveLetter( letter ) ;
            }
            catch (Exception exception)
            {
                Assert.Fail(string.Format("Editing letter caused and axception {0} with message {1}",
                    exception.GetType().Name, exception.Message));
            }
            Assert.IsNotNull( savedLetter, "SaveLetter returned null object" );
            Assert.AreEqual( initialLetter.Id, savedLetter.Id, "SaveLetter: Id changed");
            Assert.AreNotEqual(initialLetter.Date, savedLetter.Date, "SaveLetter: Date hasn't changed" );
            if (employee_check)
            {
                Assert.AreNotEqual(initialLetter.Receiver, savedLetter.Receiver, "SaveLetter: Receiver hasn't changed");
                Assert.AreNotEqual(initialLetter.Sender, savedLetter.Sender, "SaveLetter: Sender hasn't changed");
            }
            if( name_check )
                Assert.AreNotEqual(initialLetter.Name, savedLetter.Name, "SaveLetter: Name hasn't changed" );

            if( text_check )
                Assert.AreNotEqual(initialLetter.Contents, savedLetter.Contents,"SaveLetter: Contents hasn't changed" );

        }
    }
}
