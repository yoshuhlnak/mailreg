﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Test.MailServiceReference {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="MailRegistration", ConfigurationName="MailServiceReference.IDataService")]
    public interface IDataService {
        
        [System.ServiceModel.OperationContractAttribute(Action="MailRegistration/IDataService/GetAllLetters", ReplyAction="MailRegistration/IDataService/GetAllLettersResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(MailRegistration.Exception.DataBaseFault), Action="MailRegistration/IDataService/GetAllLettersDataBaseFaultFault", Name="DataBaseFault", Namespace="http://schemas.datacontract.org/2004/07/MailRegistration.Exception")]
        System.Collections.Generic.List<MailRegistration.Model.LetterBase> GetAllLetters();
        
        [System.ServiceModel.OperationContractAttribute(Action="MailRegistration/IDataService/SaveLetter", ReplyAction="MailRegistration/IDataService/SaveLetterResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(MailRegistration.Exception.DataBaseFault), Action="MailRegistration/IDataService/SaveLetterDataBaseFaultFault", Name="DataBaseFault", Namespace="http://schemas.datacontract.org/2004/07/MailRegistration.Exception")]
        MailRegistration.Model.LetterBase SaveLetter(MailRegistration.Model.LetterBase letter);
        
        [System.ServiceModel.OperationContractAttribute(Action="MailRegistration/IDataService/SaveEmployee", ReplyAction="MailRegistration/IDataService/SaveEmployeeResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(MailRegistration.Exception.DataBaseFault), Action="MailRegistration/IDataService/SaveEmployeeDataBaseFaultFault", Name="DataBaseFault", Namespace="http://schemas.datacontract.org/2004/07/MailRegistration.Exception")]
        void SaveEmployee(MailRegistration.Model.EmployeeBase employee);
        
        [System.ServiceModel.OperationContractAttribute(Action="MailRegistration/IDataService/GetAllEmployees", ReplyAction="MailRegistration/IDataService/GetAllEmployeesResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(MailRegistration.Exception.DataBaseFault), Action="MailRegistration/IDataService/GetAllEmployeesDataBaseFaultFault", Name="DataBaseFault", Namespace="http://schemas.datacontract.org/2004/07/MailRegistration.Exception")]
        System.Collections.Generic.List<MailRegistration.Model.EmployeeBase> GetAllEmployees();
        
        [System.ServiceModel.OperationContractAttribute(Action="MailRegistration/IDataService/DeleteLetter", ReplyAction="MailRegistration/IDataService/DeleteLetterResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(MailRegistration.Exception.DataBaseFault), Action="MailRegistration/IDataService/DeleteLetterDataBaseFaultFault", Name="DataBaseFault", Namespace="http://schemas.datacontract.org/2004/07/MailRegistration.Exception")]
        void DeleteLetter(MailRegistration.Model.LetterBase letter);
        
        [System.ServiceModel.OperationContractAttribute(Action="MailRegistration/IDataService/CreateNewLetter", ReplyAction="MailRegistration/IDataService/CreateNewLetterResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(MailRegistration.Exception.DataBaseFault), Action="MailRegistration/IDataService/CreateNewLetterDataBaseFaultFault", Name="DataBaseFault", Namespace="http://schemas.datacontract.org/2004/07/MailRegistration.Exception")]
        MailRegistration.Model.LetterBase CreateNewLetter();
        
        [System.ServiceModel.OperationContractAttribute(Action="MailRegistration/IDataService/GetEmployee", ReplyAction="MailRegistration/IDataService/GetEmployeeResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(MailRegistration.Exception.DataBaseFault), Action="MailRegistration/IDataService/GetEmployeeDataBaseFaultFault", Name="DataBaseFault", Namespace="http://schemas.datacontract.org/2004/07/MailRegistration.Exception")]
        MailRegistration.Model.EmployeeBase GetEmployee(int id);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IDataServiceChannel : Test.MailServiceReference.IDataService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class DataServiceClient : System.ServiceModel.ClientBase<Test.MailServiceReference.IDataService>, Test.MailServiceReference.IDataService {
        
        public DataServiceClient() {
        }
        
        public DataServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public DataServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public DataServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public DataServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public System.Collections.Generic.List<MailRegistration.Model.LetterBase> GetAllLetters() {
            return base.Channel.GetAllLetters();
        }
        
        public MailRegistration.Model.LetterBase SaveLetter(MailRegistration.Model.LetterBase letter) {
            return base.Channel.SaveLetter(letter);
        }
        
        public void SaveEmployee(MailRegistration.Model.EmployeeBase employee) {
            base.Channel.SaveEmployee(employee);
        }
        
        public System.Collections.Generic.List<MailRegistration.Model.EmployeeBase> GetAllEmployees() {
            return base.Channel.GetAllEmployees();
        }
        
        public void DeleteLetter(MailRegistration.Model.LetterBase letter) {
            base.Channel.DeleteLetter(letter);
        }
        
        public MailRegistration.Model.LetterBase CreateNewLetter() {
            return base.Channel.CreateNewLetter();
        }
        
        public MailRegistration.Model.EmployeeBase GetEmployee(int id) {
            return base.Channel.GetEmployee(id);
        }
    }
}
