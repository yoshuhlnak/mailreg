﻿using MailRegistration.Model;

namespace MailRegistration.Helpers
{
    // хелпер для работы с ILetter, LetterBase
    public static class LetterHelper
    {
        /// <summary>
        /// Получает объект LetterBase из ILetter
        /// </summary>
        /// <param name="letter"></param>
        /// <returns></returns>
        public static LetterBase ToLetterBase(this ILetter letter)
        {
            return new LetterBase()
            {
                Id = letter.Id,
                Name = letter.Name,
                Contents = letter.Contents,
                Date = letter.Date,
                Receiver = letter.Receiver.ToEmployeeBase(),
                Sender = letter.Sender.ToEmployeeBase()
            };
        }

        public static void CloneFrom(this ILetter letter, ILetter source)
        {
            letter.Id = source.Id;
            letter.Name = source.Name;
            letter.Date = source.Date;
            letter.Contents = source.Contents;
            letter.Receiver = source.Receiver;
            letter.Sender = source.Sender;
        }

    }
}