﻿using MailRegistration.Model;

namespace MailRegistration.Helpers
{
    // хелпер для работы с INamedEntity, EmployeeBase
    public static class EmployeeHelper
    {
        /// <summary>
        /// Получает объект EmployeeBase из INamedEntity
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public static EmployeeBase ToEmployeeBase(this INamedEntity employee)
        {
            return new EmployeeBase(){Id = employee.Id, Name = employee.Name };
        }
    }
}