﻿using System.Runtime.Serialization;

namespace MailRegistration.Exception
{
    [DataContract]
    public class DataBaseFault
    {
        [DataMember]
        public string Error { get; set; }

        public DataBaseFault()
        {
        }

        public DataBaseFault(string error)
        {
            Error = error;
        }
    }
}