﻿using System.Collections.Generic;
using System.ServiceModel;
using MailRegistration.Exception;
using MailRegistration.Model;

namespace MailRegistration
{
    /// <summary>
    /// Интерфейс доступа к данным
    /// </summary>
    [ServiceContract( Namespace = "MailRegistration" )]
    [ServiceKnownType( typeof( EmployeeBase ) )]
    [ServiceKnownType(typeof(LetterBase))]
    public interface IDataService
    {
        /// <summary>
        /// Получение писем по условию
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(DataBaseFault))]
        IList<LetterBase> GetAllLetters();
        /// <summary>
        /// Сохранение письма
        /// </summary>
        /// <param name="letter"></param>
        [OperationContract]
        [FaultContract(typeof(DataBaseFault))]
        LetterBase SaveLetter( LetterBase letter );

        /// <summary>
        /// Сохранение сотрудника
        /// </summary>
        /// <param name="employee"></param>
        [OperationContract]
        [FaultContract(typeof(DataBaseFault))]
        void SaveEmployee(EmployeeBase employee);

        /// <summary>
        /// Получение списка сотрудников по условию
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(DataBaseFault))]
        IList<EmployeeBase> GetAllEmployees();

        /// <summary>
        /// Удаление письма
        /// </summary>
        /// <param name="letter"></param>
        [OperationContract]
        [FaultContract(typeof(DataBaseFault))]
        void DeleteLetter(LetterBase letter);

        /// <summary>
        /// Создает пустое письмо
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(DataBaseFault))]
        LetterBase CreateNewLetter();
        /// <summary>
        /// получение сотрудника по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(DataBaseFault))]
        EmployeeBase GetEmployee( int id ); 
    }
}