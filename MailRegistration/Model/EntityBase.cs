﻿using System;
using System.Runtime.Serialization;

namespace MailRegistration.Model
{
    /// <summary>
    ///     Базовая модель сущностей.
    /// </summary>
    [DataContract( IsReference = true )]
    public abstract class EntityBase : IEquatable<EntityBase>
    {
        /// <summary>
        /// уникальный идентификатор
        /// </summary>
        [DataMember]
        public virtual int Id { get; set; }

       
        /// <summary>
        ///     Возвращает <see cref="bool.True" />, если сущность еще не закреплена на физическом уровне.
        /// </summary>
        public bool IsTransient
        {
            get { return Id == 0; }
        }

        #region Методы для сравнения сущностей
        public override bool Equals( object other )
        {
            return Equals( other as EntityBase );
        }

        public override int GetHashCode()
        {
            if( IsTransient )
                return base . GetHashCode();
            return Id;
        }

        public virtual bool Equals( EntityBase other )
        {
            if( other == null )
                return false;
            if( IsTransient )
                return ReferenceEquals( this, other );
            return other . Id == Id && other . GetType() == GetType();
        }
        #endregion
    }

}