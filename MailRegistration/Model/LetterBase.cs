﻿using System;
using System.Runtime.Serialization;

namespace MailRegistration.Model
{
    // DTO объект для передачи сервисом
    [DataContract(IsReference = true)]
    public class LetterBase : EntityBase, ILetter
    {
        [DataMember]
        public override int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public string Contents { get; set; }
        [DataMember]
        public virtual EmployeeBase Receiver { get; set; }
        [DataMember]
        public virtual EmployeeBase Sender { get; set; }

        INamedEntity ILetter.Receiver
        {
            get { return Receiver; }
            set { Receiver = value as EmployeeBase; }
        }

        INamedEntity ILetter.Sender {
            get { return Sender; }
            set { Sender = value as EmployeeBase; }
        }
    }
}