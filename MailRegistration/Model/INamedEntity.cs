﻿namespace MailRegistration.Model
{
    /// <summary>
    /// Интерфейс для справочников
    /// </summary>
    public interface INamedEntity : IEntity
    {
        string Name { get; set; } 
    }
}