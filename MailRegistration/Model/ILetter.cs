﻿using System;

namespace MailRegistration.Model
{
    public interface ILetter : INamedEntity
    {
        DateTime Date { get; set; }
        INamedEntity Receiver { get; set; }
        INamedEntity Sender { get; set; }
        string Contents { get; set;  }
    }
}