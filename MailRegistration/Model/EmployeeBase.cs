﻿using System.Runtime.Serialization;

namespace MailRegistration. Model
{
    // DTO объект для передачи сервисом
    [DataContract( IsReference = true )]
    public class EmployeeBase : EntityBase, INamedEntity
    {
        [DataMember]
        public override int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
    }
}
