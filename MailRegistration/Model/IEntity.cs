﻿namespace MailRegistration.Model
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}