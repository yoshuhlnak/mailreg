﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using MailRegistration.Model;
using PropertyChanged;

namespace MailRegApp.ViewModel
{
    public interface ILetterContentsViewModel : IValidatableViewModel, ILetter
    {
        string ReceiverName { get; }
        string SenderName { get; }
    }

    /// <summary>
    /// view model, отображающее содержимое письма
    /// </summary>
    [ImplementPropertyChanged]
    public class LetterContentsViewModel : ViewModel, ILetterContentsViewModel, IDataErrorInfo
    {
        public int Id { get; set; }

        public LetterContentsViewModel()
        {
            // устанавливаем дату по-умолчанию
            Date = DateTime.Today;
        }

        public INamedEntity Sender { get; set; }

        public string Name { get; set; }

        public DateTime Date { get; set; }

        public INamedEntity Receiver { get; set;  }

        public string Contents { get; set; }

        [DependsOn("Receiver")] // вызывает PropertyChanged для данного свойства при изменении Receiver'а
        public string ReceiverName 
        { 
            get { return Receiver != null ? Receiver.Name : ""; } 
        }
        [DependsOn( "Sender" )]// вызывает PropertyChanged для данного свойства при изменении Sender'а
        public string SenderName 
        {
            get { return Sender != null ? Sender.Name : ""; }
        }
        /// <summary>
        /// валидация данных через IDataErrorInfo.this[PropertyName]
        /// </summary>
        /// <returns></returns>
        public bool Validate()
        {
            return string . IsNullOrEmpty( this[ "Name" ] ) && string . IsNullOrEmpty( this[ "Sender" ] )
                   && string . IsNullOrEmpty( this[ "Receiver" ] ) && string . IsNullOrEmpty( this[ "Contents" ] );
        }

        public IList<INamedEntity> EmployeesList { get; set; }
        #region IDataErrorInfo implementation

        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Name":
                    {
                        if (string.IsNullOrEmpty(Name))
                            return "Поле \"Наименование\" не может быть пустым";
                        if (Name.Length > 1000)
                            return string.Format(
                                "Превышена длина поля \"Наименование\". Введено {0} символа(ов) из {1} возможных.",
                                Name.Length, 1000);
                        break;
                    }
                    case "Sender":
                    {
                        if (Sender == null)
                            return "Поле \"Отправитель\" не может быть пустым";
                        break;
                    }
                    case "Receiver":
                    {
                        if (Receiver == null)
                            return "Поле \"Получатель\" не может быть пустым";
                       
                        break;
                    }
                    case "Contents":
                    {
                        if (string.IsNullOrEmpty(Contents))
                            return "Поле \"Содержание\" не может быть пустым";
                        if (Contents.Length > 8000)
                            return string.Format(
                                "Превышена длина поля \"Содержание\". Введено {0} символа(ов) из {1} возможных.",
                                Name.Length, 8000);
                        break;
                    }
                }
                return null;
            }
        }

        public string Error { get; private set; }
#endregion

    }
}