﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using MailRegApp.Command;
using MailRegApp.Model;
using MailRegApp.Service;
using MailRegApp.ViewModel.Factory;
using MailRegistration.Helpers;
using MailRegistration.Model;
using PropertyChanged;

namespace MailRegApp.ViewModel
{
    [ImplementPropertyChanged]
    public class LetterListProgressableViewModel : ProgressableViewModel
    {
        private readonly ILocalDataService _dataService;
        private readonly ILetterContentsViewModelFactory _letterContentsViewModelFactory;

        public LetterListProgressableViewModel(ILocalDataService dataService, IDialogService dialogService,
            ILetterContentsViewModelFactory letterContentsViewModelFactory):base( dialogService )
        {
            _dataService = dataService;
            _letterContentsViewModelFactory = letterContentsViewModelFactory;
            Letters = new ObservableCollection<ILetter>();
        }

        public IList<ILetter> Letters { get; set; } // Коллекция писем 
        public ILetter FocusedLetter { get; set; } // Выбранное письмо из коллекции
        public bool IsBusy { get; set; } // Признак для ассинхронных операций

        public ICommand AddCommand
        {
            get { return new RelayCommand(AddNewLetter, () => true); }
        }

        public ICommand DeleteCommand
        {
            get { return new RelayCommand(DeleteLetter, CanDelete); }
        }

        public ICommand EditCommand
        {
            get { return new RelayCommand(EditLetter, CanEdit); }
        }

        public ICommand RefreshCommand
        {
            get { return new RelayCommand(Refresh, () => true); }
        }

        /// <summary>
        /// Добавление нового письма в диалоговом окне
        /// </summary>
        private void AddNewLetter()
        {
            LetterContentsViewModel vm = null;
            DoWithProgress(() => vm = _letterContentsViewModelFactory.Get());
            if (DialogService.ShowSaveCancelModal(vm))
            {
                DoWithProgress(() =>
                {
                    //создаем пустое письмо
                    ILetter letter = _dataService.CreateNewLetter();
                    //переносим данные из viewmodel 
                    letter.CloneFrom(vm);
                    // сохраняем письмо и добавляем в коллекцию
                    letter = new ObservableLetter( _dataService.SaveLetter(letter)) ;
                    Letters.Add(letter);
                });
            }
        }

        private bool CanDelete()
        {
            return FocusedLetter != null;
        }

        /// <summary>
        /// удаление из источника данных
        /// </summary>
        private void DeleteLetter()
        {

            ILetter letter = FocusedLetter; // сохраняем, т.к. после удаления из листа FocusedLetter == null
            // удаление из коллекции, отображаемой в UI
            Letters.Remove(FocusedLetter);
            // удаление из источника данных
            DoWithProgress(() =>
               _dataService.DeleteLetter(letter)
            );
        }

        private bool CanEdit()
        {
            return FocusedLetter != null;
        }

        // Редактирование выделенного письма в диалоговом окне
        private void EditLetter()
        {
            LetterContentsViewModel vm = null; 
            //создаем и заполняем viewmodel из выбранного письма
            DoWithProgress(() =>
            {
                vm = _letterContentsViewModelFactory.Get();
                vm.CloneFrom(FocusedLetter);
            });
            //показываем пользователю
            if (DialogService.ShowSaveCancelModal(vm))
            {
                LetterBase letter = new LetterBase();
                //переносим данные из viewmodel в письмо
                letter.CloneFrom(vm);
                // сохраняем 
                DoWithProgress(() =>
                {
                    ILetter newLetter = new ObservableLetter(_dataService . SaveLetter( letter ));
                    FocusedLetter.CloneFrom(newLetter);    
                });
            }
        }

        /// <summary>
        /// Обновление коллекции, связанной с UI из источника данных
        /// </summary>
        private void Refresh()
        {
            FocusedLetter = null;
            IList<ILetter> letters = null;
            DoWithProgress(() => 
                letters = _dataService . GetAllLetters());
            Letters = letters != null
                ? new ObservableCollection<ILetter>( letters.Select( letter => new ObservableLetter(letter) ) )
                : new ObservableCollection<ILetter>();
        }
    }
}