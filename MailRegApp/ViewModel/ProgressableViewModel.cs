﻿using System;
using MailRegApp.Service;

namespace MailRegApp.ViewModel
{
    /// <summary>
    /// Базовый класс для viewmodel с автоматическим запуском progress bar до действия и автоматическим закрытием после
    /// </summary>
    public class ProgressableViewModel : ViewModel
    {
        protected readonly IDialogService DialogService;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dialogService">Сервис, позволяющий вызывать/закрывать progress bar</param>
        protected ProgressableViewModel(IDialogService dialogService)
        {
            DialogService = dialogService;
        }

        /// <summary>
        /// Показывает прогресс бар, потом выполняет действие и закрывает прогресс бар
        /// </summary>
        /// <param name="action">Выполняемое действие</param>
        /// <param name="onFail">Действия при возникновении исключений</param>
        protected void DoWithProgress( Action action, Action<Exception> onFail = null )
        {
             DialogService.BeginWaiting();
            try
            {
                action();
            }
            catch (Exception exception)
            {
                if (onFail != null)
                    onFail(exception);
                else
                    OnException( exception );
            }
            finally
            {
                DialogService.EndWaiting();
            }
        }
        /// <summary>
        /// Обработка исключения. Показывает message box пользователю с сообщением в переменной exception
        /// </summary>
        /// <param name="exception"></param>
        protected virtual void OnException(Exception exception)
        {
            DialogService . ShowMessage( string . Format( "Операция вызвала ошибку {0}:\n{1}", exception . GetType() . Name, exception . Message ) );
        }
    }
}