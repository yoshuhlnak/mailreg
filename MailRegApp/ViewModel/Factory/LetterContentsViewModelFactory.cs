﻿using System.Collections.Generic;
using MailRegApp.Service;
using MailRegistration;
using MailRegistration.Model;

namespace MailRegApp.ViewModel.Factory
{
    // фабрика для создания view model с содержанием письма
    public class LetterContentsViewModelFactory : ILetterContentsViewModelFactory
    {
        private readonly ILocalDataService _dataService;

        public LetterContentsViewModelFactory( ILocalDataService dataService )
        {
            _dataService = dataService;
        }
        /// <summary>
        /// создает новый экземпляр LetterContentsViewModel
        /// </summary>
        /// <returns></returns>
        public LetterContentsViewModel Get()
        {
            var viewModel = new LetterContentsViewModel();
            // получаем список сотрудников
            IList<INamedEntity> list = new List<INamedEntity>();
            viewModel.EmployeesList = _dataService.GetAllEmployees();
            return viewModel;
        }
    }
}