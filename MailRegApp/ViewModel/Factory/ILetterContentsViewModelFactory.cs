﻿namespace MailRegApp.ViewModel.Factory
{
    /// <summary>
    /// интерфейс фабрики для создания view model с содержанием письма
    /// </summary>
    public interface ILetterContentsViewModelFactory
    {
        LetterContentsViewModel Get();
    }
}