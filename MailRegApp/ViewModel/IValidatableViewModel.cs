namespace MailRegApp.ViewModel
{
    /// <summary>
    /// ��������� ��� view model, �������������� ���������
    /// </summary>
    public interface IValidatableViewModel
    {
        bool Validate();
    }
}