﻿using System;
using System.Windows.Input;
using MailRegApp.Command;
using PropertyChanged;

namespace MailRegApp.ViewModel
{
    /// <summary>
    /// view model для диалогового окна с возможностями сохранить и отменить 
    /// </summary>
    [ImplementPropertyChanged]
    public class SaveCancelDialogViewModel : ViewModel
    {
        private readonly Func<bool> _canSaveDelegate;

        public ViewModel ViewModel { get; set; }

        /// <summary>
        /// свойство, которое биндится к одноименному свойству модального окна
        /// </summary>
        public bool? DialogResult { get; set; }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="viewModel">view model с данными, которые должно отобразить диалоговое окно</param>
        /// <param name="canSaveDelegate">делегат метода, определяющего доступность кнопки сохранить</param>
        public SaveCancelDialogViewModel(ViewModel viewModel, Func<bool> canSaveDelegate)
        {
            if (viewModel == null)
                throw new ArgumentNullException(
                    string.Format("child viewModel of SaveCancelDialogViewModel cannot be null"));
            ViewModel = viewModel;
            _canSaveDelegate = canSaveDelegate;
        }
        /// <summary>
        /// Действия при сохранении
        /// </summary>
        public ICommand SaveCommand {
            get {  return new RelayCommand( () => { DialogResult  = true ; }, _canSaveDelegate ) ; }
        }
        /// <summary>
        /// Действия при отмене
        /// </summary>
        public ICommand CancelCommand {
            get { return new RelayCommand(() => { DialogResult = false; }, () => true); }
        }

    }
}