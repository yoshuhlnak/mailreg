﻿using System;
using System.Windows;
using System.ServiceModel;
using System.ServiceModel.Description;
using MailRegApp.View;
using MailRegistration;

namespace MailRegApp
{
    public class Program
    {
        private static ServiceHost _host; 

        [STAThread]
        public static void Main(string[] args)
        {
            try
            {
                StartDataService(); 
                App.Main();
            }
            catch (CommunicationException ce)
            {
                Console.WriteLine("Cannot start service. Exception occured: \n{0}", ce.Message);
                _host.Abort();
            }
            catch (Exception exception)
            {
                OnException(exception);
            }

            // Close the ServiceHostBase to shutdown the service.
            if( _host.State == CommunicationState.Opened )
                _host.Close();
        }

        static void StartDataService()
        {
            // Step 1 Create a URI to serve as the base address.
            Uri baseAddress = new Uri("http://localhost:8000/MailRegistration/");

            // Step 2 Create a ServiceHost instance
            _host = new ServiceHost(typeof(DataService.DataService), baseAddress);

            // Step 3 Add a service endpoint.
            _host.AddServiceEndpoint(typeof(IDataService), new WSHttpBinding(), "DataService");

            // Step 4 Enable metadata exchange.
            ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
            smb.HttpGetEnabled = true;

            _host.Description.Behaviors.Add(smb);

            // Step 5 Start the service.
            _host.Open();
        }

        private static void OnException(Exception exception)
        {
            try
            {
                if (Application.Current != null)
                {
                        Application.Current.Dispatcher.Invoke(
                            new Action(() =>
                                new MessageWindow(exception.Message).ShowDialog()
                                ));
                }
            }
            finally
            {
            }
        }
    }
}