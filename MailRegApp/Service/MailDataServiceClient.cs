﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using MailRegApp.DataServiceReference;
using MailRegistration;
using MailRegistration.Exception;
using MailRegistration.Helpers;
using MailRegistration.Model;
using IDataService = MailRegApp.DataServiceReference.IDataService;

namespace MailRegApp.Service
{
    public class MailDataServiceClient : ILocalDataService
    {
        private readonly DataServiceClient _client;
        private readonly IDialogService _dialogService; 

        /// <summary>
        /// Обертка над действиями с сервисом. Содержит стандартные catch и закрывает _client после выполнения действий
        /// </summary>
        /// <param name="action"></param>
        private void DoAction(Action action)
        {
            try
            {
                action.Invoke();
            }
            catch (TimeoutException exception)
            {
                _dialogService.ShowMessage(string.Format(
                    "Операция сервиса данных вернула ошибку {0}:\nТело ошибки:{1}", exception.GetType().Name,
                    exception.Message));
                _client.Abort();
            }
            catch (FaultException<DataBaseFault> exception)
            {
                _dialogService.ShowMessage(exception.Detail.Error);
                _client.Abort();
            }
            catch (CommunicationException exception)
            {
                _dialogService.ShowMessage(string.Format(
                    "Операция сервиса данных вернула ошибку {0}:\nТело ошибки:{1}", exception.GetType().Name,
                    exception.Message));
                _client.Abort();
            }
        }

        public MailDataServiceClient(DataServiceClient client, IDialogService dialogService)
        {
            _client = client;
            _dialogService = dialogService;
        }

        #region ILocalDataService
        public IList<ILetter> GetAllLetters()
        {
            var letters = (this as IDataService).GetAllLetters();
            return letters != null ? letters.OfType<ILetter>().ToList() : null ;
        }

        IList<INamedEntity> ILocalDataService.GetAllEmployees()
        {
            List<EmployeeBase> employees = (this as IDataService).GetAllEmployees();
            return employees != null ? employees.OfType<INamedEntity>().ToList() : null;
        }

        public void DeleteLetter(ILetter letter)
        {
            DeleteLetter( letter.ToLetterBase() );
        }

        public LetterBase SaveLetter(ILetter letter)
        {
            return SaveLetter( letter.ToLetterBase() );
        }
 #endregion

#region IDataService
        List<LetterBase> IDataService.GetAllLetters()
        {
            List<LetterBase> letters = null;
            DoAction(() => letters = _client.GetAllLetters());
            return letters;
        }

        public LetterBase SaveLetter( LetterBase letter )
        {
            LetterBase letterBase = null;
            DoAction( () => letterBase = _client.SaveLetter(letter) );
            return letterBase;
        }

        public void SaveEmployee(EmployeeBase employee)
        {
            DoAction(() => _client.SaveEmployee( employee ) );
        }

        public List<EmployeeBase> GetAllEmployees()
        {
            List<EmployeeBase> employees = null;
            DoAction(() => employees = _client.GetAllEmployees());
            return employees;
        }

        public void DeleteLetter(LetterBase letter)
        {
            DoAction(() => _client.DeleteLetter(letter));
        }

        public LetterBase CreateNewLetter()
        {
            LetterBase letter = null;
            DoAction(() => letter = _client.CreateNewLetter());
            return letter;
        }

        public EmployeeBase GetEmployee( int id )
        {
            EmployeeBase employee = null;
            DoAction(() => employee = _client.GetEmployee(id) );
            return employee; 
        }
#endregion
    }
}