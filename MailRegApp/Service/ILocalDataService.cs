﻿using System.Collections.Generic;
using MailRegistration.Model;
using IDataService = MailRegApp.DataServiceReference.IDataService;

namespace MailRegApp.Service
{
    /// <summary>
    /// Надстройка над IDataService для работы с интерфейсом ILetter
    /// </summary>
    public interface ILocalDataService : IDataService
    {
        void DeleteLetter(ILetter letter);
        LetterBase SaveLetter(ILetter letter);
        new IList<ILetter> GetAllLetters();
        new IList<INamedEntity> GetAllEmployees();
    }
}