﻿using System.Security.Cryptography.X509Certificates;

namespace MailRegApp.Service
{
    public interface IDialogService
    {
        ProgressManager ProgressManager { get; }

        /// <summary>
        /// Отображение диалогового окна с кнопками Save и Cancel
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        bool ShowSaveCancelModal<T>( T viewModel ) where T : ViewModel.ViewModel;

        void ShowMessage(string message);

        void BeginWaiting();
        void EndWaiting();
    }
}