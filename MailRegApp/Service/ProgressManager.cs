﻿using System;
using System.Threading;
using System.Windows.Threading;
using MailRegApp.View.DialogWindows;

namespace MailRegApp.Service
{
    /// <summary>
    /// Управление диалоговым окном с progress bar
    /// </summary>
    public class ProgressManager
    {
        private Thread _thread;
        private bool _canAbortThread = false;
        private ProgressWindow _window;

        public void BeginWaiting()
        {
            this . _thread = new Thread( this . RunThread );
            this . _thread . IsBackground = true;
            this . _thread . SetApartmentState( ApartmentState . STA );
            this . _thread . Start();
           
        }
        public void EndWaiting()
        {
            if( this . _window != null )
            {
                this . _window . Dispatcher . BeginInvoke( DispatcherPriority . Normal, ( Action ) ( () =>
                { this . _window . Close(); } ) );
                while( !this . _canAbortThread ) { };
            }
            this . _thread . Abort();           
        }

        public void RunThread()
        {
            this._window = new ProgressWindow();
            this._window.Closed += new EventHandler(waitingWindow_Closed);
            this . _window . ShowDialog();
           
        }
        public void ChangeStatus( string text )
        {
            if( this . _window != null )
            {
                this . _window . Dispatcher . BeginInvoke( DispatcherPriority . Normal, ( Action ) ( () =>
                { this . _window . Title = text; } ) );
            }
        }

        void waitingWindow_Closed( object sender, EventArgs e )
        {
            Dispatcher . CurrentDispatcher . InvokeShutdown();
            this . _canAbortThread = true;
        }
    }
}