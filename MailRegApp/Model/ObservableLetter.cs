﻿using System;
using MailRegistration.Helpers;
using MailRegistration.Model;
using PropertyChanged;

namespace MailRegApp.Model
{
    /// <summary>
    /// Реализация ILetter с INotifiyPropertyChanged
    /// </summary>
    [ImplementPropertyChanged]
    public class ObservableLetter : ILetter
    {
        public ObservableLetter( ILetter letter )
        {
            this . CloneFrom( letter );
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public INamedEntity Receiver { get; set; }
        public INamedEntity Sender { get; set; }
        public string Contents { get; set; }
    }
}