﻿using System.Windows;
using MailRegApp.DataServiceReference;
using MailRegApp.Service;
using MailRegApp.ViewModel;
using MailRegApp.ViewModel.Factory;

namespace MailRegApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private MailDataServiceClient _dataService;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var mainWindow = new MainWindow();
            var dataServiceReference = new DataServiceClient();
            _dataService = new MailDataServiceClient(dataServiceReference, mainWindow);
            var letterContentsVmFactory = new LetterContentsViewModelFactory( _dataService );
            var viewModel = new LetterListProgressableViewModel( _dataService, mainWindow, letterContentsVmFactory );
            mainWindow.DataContext = viewModel;
            mainWindow.ShowDialog();
        }
    }
}
