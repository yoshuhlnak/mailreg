﻿using System.Windows;

namespace MailRegApp.Assets
{
    /// <summary>
    /// Реализация attached property, чтобы можно было биндить к нему свойства из view model и тем самым закрывать диалоговое окно установкой этого свойства
    /// </summary>
    public class DialogCloser
    {
        public static readonly DependencyProperty DialogResultProperty =
           DependencyProperty . RegisterAttached(
               "DialogResult",
               typeof( bool? ),
               typeof( DialogCloser ),
               new PropertyMetadata( DialogResultChanged ) );

        private static void DialogResultChanged(
            DependencyObject d,
            DependencyPropertyChangedEventArgs e )
        {
            var window = d as Window;
            if( window != null )
                window . DialogResult = e . NewValue as bool?;
        }
        public static void SetDialogResult( Window target, bool? value )
        {
            target . SetValue( DialogResultProperty, value );
        } 
    }
}