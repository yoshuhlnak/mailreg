﻿using System;
using System.Windows;
using MailRegApp.Service;
using MailRegApp.View;
using MailRegApp.ViewModel;

namespace MailRegApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IDialogService
    {
        public MainWindow()
        {
            InitializeComponent();
           // ProgressManager = new ProgressManager();
        }

        public ProgressManager ProgressManager { get; private set; }

        public bool ShowSaveCancelModal<T>(T viewModel) where T : ViewModel.ViewModel
        {
            if( viewModel == null )
                throw new ArgumentNullException( "Imposible to show dialog window without view model" );
            // создаем окно
            var wnd = new SaveCancelDialogWindow()
                {Owner = this};
            // todo: устанавливаем размеры диалогового окна
            // создаем диалоговую viewmodel 
            SaveCancelDialogViewModel dialogViewModel;
            if( viewModel is IValidatableViewModel )
                // если view model поддерживает валидацию - Save должна быть доступна только в случае успешной валидации
                dialogViewModel = new SaveCancelDialogViewModel( viewModel,( (IValidatableViewModel) viewModel ) . Validate );
            else
                // в другом случае - save доступна всегда
                dialogViewModel = new SaveCancelDialogViewModel( viewModel, () => true);

            wnd.DataContext = dialogViewModel; 
            return wnd.ShowDialog()??false; 
        }

        public void ShowMessage(string message)
        {
            MessageWindow wnd = new MessageWindow(message);
            wnd.Show();
        }

        public void BeginWaiting()
        {
            if (ProgressManager == null)
            {
                ProgressManager = new ProgressManager();
                ProgressManager.BeginWaiting();
            }
        }

        public void EndWaiting()
        {
            if (ProgressManager != null)
            {
                ProgressManager.EndWaiting();
                ProgressManager = null;
            }
        }
    }
}
