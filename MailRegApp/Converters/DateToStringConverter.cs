﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace MailRegApp.Converters
{
    public class DateToStringConverter : MarkupExtension, IValueConverter
    {
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if( value == null )
                throw new ArgumentNullException( "Передано неопределенное значение" );
            if( !( value is DateTime ) )
                throw new ArgumentException( "Переданное значение не является значением типа DateTime" );
            return ((DateTime) value).ToShortDateString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}