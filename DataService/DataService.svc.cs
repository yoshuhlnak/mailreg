﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.ServiceModel;
using DAL;
using MailRegistration;
using MailRegistration.Exception;
using MailRegistration.Helpers;
using MailRegistration.Model;

namespace DataService
{
    public class DataService : IDataService
    {
        #region private members

        private void DoAction(Action action)
        {
            try
            {
                action.Invoke();
            }
            catch (Exception exception)
            {
                string msg = string.Format("При выполнении операции возникла ошибка {0}/nТело ошибки: {1}",
                    exception.GetType().Name, exception.Message);
                throw new FaultException<DataBaseFault>(new DataBaseFault(msg));
            }
        }

        /// <summary>
        /// Преобразуем из интерфейса в объект контекста
        /// </summary>
        /// <param name="letter"></param>
        /// <returns></returns>
        private Letter ConvertToLetter( ILetter letter )
        {
            return new Letter()
            {
                Id = letter . Id,
                Name = letter . Name,
                Date = letter . Date,
                Contents = letter . Contents,
                ReceiveEmployeeId = letter.Receiver.Id,
                SendEmployeeId = letter.Sender.Id,
                Receiver = ConvertToEmployee( letter . Receiver ),
                Sender = ConvertToEmployee( letter . Sender )
            };
        }
        /// <summary>Преобразуем из интерфейса в объект контекста
        /// 
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        private Employee ConvertToEmployee( INamedEntity employee )
        {
            return new Employee()
            {
                Id = employee . Id,
                Name = employee . Name
            };
        }
        /// <summary>
        /// Проверка на существование в локальном кэше
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        private bool Exists<T>(DbContext context, T entity) where T : class
        {
            return context.Set<T>().Local.Any(e => e == entity);
        }
        #endregion
        public DataService()
        {
        }

        /// <summary>
        /// Получение списка писем по условию
        /// </summary>
        /// <returns></returns>
        public IList<LetterBase> GetAllLetters()
        {
            IList<LetterBase> letters = null;
            DoAction(() =>
            {
                using (DataContext context = new DataContext())
                {
                    // при выборке сразу создаем DTO объекты, чтобы сервис мог их передать
                    letters = context.Letters
                        .AsNoTracking()
                        .Select(
                            letter =>
                                new LetterBase()
                                {
                                    Id = letter.Id,
                                    Name = letter.Name,
                                    Contents = letter.Contents,
                                    Date = letter.Date,
                                    Receiver =
                                    // в том числе преобразуем в DTO свойства-navprops
                                        new EmployeeBase()
                                        {
                                            Id = letter.ReceiveEmployee.Id,
                                            Name = letter.ReceiveEmployee.Name
                                        },
                                    Sender =
                                        new EmployeeBase()
                                        {
                                            Id = letter.SendEmployee.Id,
                                            Name = letter.SendEmployee.Name
                                        }
                                })
                        .ToList();
                }
            }
    );
            return letters;
        }
        /// <summary>
        /// сохранение сущности письмо
        /// </summary>
        /// <param name="letter"></param>
        public LetterBase SaveLetter( LetterBase letter )
        {
            Letter dbLetter = null;
            DoAction(() =>
            {

                dbLetter = ConvertToLetter(letter); 
                // объекты в базе - update, новые - insert
                using (DataContext context = new DataContext())
                {
                    if (!dbLetter.ReceiveEmployee.IsTransient && !Exists(context,dbLetter.ReceiveEmployee))
                    {
                        context.Employees.Attach(dbLetter.ReceiveEmployee);
                    }
                    if (!dbLetter.SendEmployee.IsTransient && !Exists(context, dbLetter.SendEmployee))
                    {
                        context.Employees.Attach(dbLetter.SendEmployee);
                    }
                    if (dbLetter.Id > 0) // если id > 0 значит объект уже должен быть в базе
                    {
                        context.Letters.Attach(dbLetter);
                        context.Entry(dbLetter).State = EntityState.Modified;
                    }
                    else
                    {
                        context.Letters.Add(dbLetter);
                    }
                    context.SaveChanges();
                }
            });
            return dbLetter.ToLetterBase() ;
        }

        /// <summary>
        /// Сохранение сущности сотрудник
        /// </summary>
        /// <param name="employee"></param>
        public void SaveEmployee(EmployeeBase employee)
        {
            DoAction(() =>
            {
                Employee dbEmployee = ConvertToEmployee(employee);
                using (DataContext context = new DataContext())
                {
                    // объекты в базе - update, новые - insert
                    if (dbEmployee.Id > 0) // если id > 0 значит объект уже должен быть в базе
                    {
                        context.Employees.Attach(dbEmployee);
                        context.Entry(dbEmployee).State = EntityState.Modified;
                    }
                    else
                    {
                        context.Employees.Add(dbEmployee);
                    }
                    context.SaveChanges();
                }
            });
        }

        /// <summary>
        /// Получение сотрудников по условию
        /// </summary>
        /// <returns></returns>
        public IList<EmployeeBase> GetAllEmployees()
        {
            IList<EmployeeBase> employeeList = null;
            DoAction(() =>
            {
                using (DataContext context = new DataContext())
                {
                    employeeList =
                        context.Employees.AsNoTracking()
                            .Select(e => new EmployeeBase() {Id = e.Id, Name = e.Name})
                            .ToList();
                }
            }
            );
            return employeeList;
        }
        /// <summary>
        /// Удаление письма из базы
        /// </summary>
        public void DeleteLetter(LetterBase letter)
        {
            // если объект существует в базе
            if (!letter.IsTransient)
            {
                DoAction(() =>
                {
                    Letter dbLetter = ConvertToLetter(letter);
                    using (DataContext context = new DataContext())
                    {
                        context.Letters.Attach(dbLetter);
                        context.Letters.Remove(dbLetter);
                        context.SaveChanges();
                    }
                });
            }
        }

        /// <summary>
        /// Создаем пустое письмо
        /// </summary>
        /// <returns></returns>
        public LetterBase CreateNewLetter()
        {
            return new LetterBase() { Id = 0 };
        }
        /// <summary>
        /// получение сотрудника по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public EmployeeBase GetEmployee(int id)
        {
            EmployeeBase employee = null;
            DoAction(() =>
            {
                using (DataContext context = new DataContext())
                {
                    employee =
                        context.Employees.AsNoTracking()
                            .Where(e => e.Id == id)
                            .Select(e => new EmployeeBase() {Id = e.Id, Name = e.Name})
                            .FirstOrDefault();
                }
            }
                );
            return employee;
        }
    }
}
